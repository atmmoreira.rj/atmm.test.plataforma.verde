class User {
  constructor(_name, _birth, _state, _city) {
    Object.assign(this, { _name, _birth, _state, _city, });
    Object.freeze(this);
  }

  get name() {
    return this._name;
  }

  get birth() {
    return this._birth;
  }

  get state() {
    return this._state;
  }

  get city() {
    return this._city
  }
}

export default User;
