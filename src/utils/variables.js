import axios from 'axios';
export const axiosJson = axios.get('./assets/cities/cities.json');

// Form
export const form = document.querySelector('#form')
export const name = document.querySelector('#name');
export const birth = document.querySelector('#birth');
export const states = document.querySelector('#state');
export const cities = document.querySelector('#city');

// Table
export const listNames = document.querySelector('#listNames');
