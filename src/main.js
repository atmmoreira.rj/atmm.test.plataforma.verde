import '../node_modules/bootstrap/scss/bootstrap.scss';
import './scss/style.scss';

import * as variables from './utils/variables';
import UsersControllers from './controllers/UsersControllers';

let usersController = new UsersControllers();
usersController.loadInformations();
variables.form.addEventListener('submit', usersController.init.bind(usersController));

// Loading States and Cities
variables.axiosJson
  .then(res => {
    const data = res.data.estados;
    data.forEach(element => variables.states.innerHTML += `<option value="${element.sigla}">${element.sigla}</option>`);

    variables.states.addEventListener('change', () => {
      variables.cities.innerHTML = '';
      let state = data.filter(state => state.sigla === variables.states.value), citiesFiltered = state[0].cidades;
      citiesFiltered.forEach(city => variables.cities.innerHTML += `<option value="${city}">${city}</option>`);
    });
  })
  .catch(e => console.log(e));
