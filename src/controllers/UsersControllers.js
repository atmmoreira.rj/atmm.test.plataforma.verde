import User from '../class/User';
import Users from '../model/Users';
import * as variables from '../utils/variables';
import UserViews from '../views/UsersViews';

class UsersControllers {

  constructor() {
    this._name = variables.name;
    this._birth = variables.birth;
    this._state = variables.states;
    this._city = variables.cities;

    this._users = new Users();
    this._usersViews = new UserViews();
  }

  loadInformations() {
    let listUsersAll = this._users.getStorage('Users');
    if (!!listUsersAll) this._usersViews.updateList(listUsersAll);
  }

  init(e) {
    e.preventDefault();
    if (this._name.value === '' || this._birth.value === '' || this._state.value === '' || this._city.value === '') return alert('Todos os campos são obrigatórios');
    this._users.store(this._newUser());
    this._users.setStorage();
    this._clearForm();
    this.loadInformations();
  }

  _clearForm() {
    this._name.value = '';
    this._birth.value = '';
    this._state.value = '';
    this._city.value = '';
  }

  _newUser() {
    return new User(this._name.value, this._birth.value, this._state.value, this._city.value);
  }

}

export default UsersControllers
