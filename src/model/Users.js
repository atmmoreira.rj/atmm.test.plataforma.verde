class Users {
  constructor() {
    this._users = this.getStorage('Users') || [];
  }

  store(user) {
    this._users.push(user);
  }

  setStorage() {
    return localStorage.setItem('Users', JSON.stringify(this._users));
  }

  getStorage(localstorage) {
    let users = localStorage.getItem(localstorage);
    return JSON.parse(users);
  }

  show() {
    return this._users;
  }

}

export default Users
