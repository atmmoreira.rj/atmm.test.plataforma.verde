import * as variables from '../utils/variables'

class UserViews {

  constructor() {
    this._element = variables.listNames;
  }

  template(model) {
    return `
    <tr>
      <th scope="row">${model._name}</th>
      <td>${this.formatBirth(model._birth)}</td>
      <td>${this.getAge(model._birth)}</td>
      <td>${model._state}</td>
      <td>${model._city}</td>
      <td><a href=""><i class="fa fa-edit text-warning"></i></a> <a href=""><i class="fa fa-trash text-danger"></i></a></td>
    </tr>
  `
  }

  updateList(model) {
    let usersTr = '';
    for (let m of model) {
      usersTr += this.template(m);
    }

    return this._element.innerHTML = usersTr;
  }

  formatBirth(date) {
    let dateBirth = new Date(date);
    let arrayDate = dateBirth.toLocaleDateString('pt-BR')
    return arrayDate;
  }

  getAge(date) {
    let dateBirth = new Date(date);
    let yearNow = new Date();
    let yearOfBirth = yearNow.getFullYear() - dateBirth.getFullYear();
    return yearOfBirth;
  }

}

export default UserViews
